/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/composition_vs_inheritance.js":
/*!***************************************************!*\
  !*** ./application/composition_vs_inheritance.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/*\n\n  Composition vs Inheritance\n  Компоновка (Композиция) против Наследования.\n\n  Наследование, определяет обьект по тому чем он ЯВЛЯЕТСЯ.\n  Композиция, определяет обьект по тому, что он ДЕЛАЕТ\n\n  Опишем проблематику. Слайды!\n\n*/\n// Our Functions\n\nconst Composition = () => {\n\nconst Drive = ( state ) => ({\n  drive: () => console.log('Wroooom!, It\\'s a car ' + state.name )\n});\n\nconst ChangeName = state => ({\n  changeName: ( name ) => {\n    console.log(`Old name:`, state.name, state );\n    state.name = name;\n    console.log(`New name:`, state.name, state );\n  }\n});\n\nconst Refill = ( state ) => ({\n  refill: () => console.log( state.name + ' was refiled')\n});\n\nconst Move = ( state ) => ({\n  move: ( speed ) => {\n    console.log(speed);\n    state.speed += speed ;\n    console.log( state.name + ' is moving. Speed ->' + state.speed );\n  }\n});\n\nconst Fly = ( state ) => ({\n  fly: () => {\n    console.log( state );\n    // state.name = \"qOp\";\n    console.log( state.name + ' flying into sky! Weather is ' + state.weather );\n  }\n});\n\nconst LoggerIn = obj => ({\n  logger: () => {\n    console.log( obj );\n  }\n});\n\n// Проверим ф-ю\nRefill({name: \"Volkswagen\"}).refill(); // Volkswagen was refiled\n//\n// // Наш конструктор.\nconst EcoRefillDrone = (name, speed) => {\n  let state = {\n    name,\n    speed: Number(speed),\n    weather: 'rainy'\n  };\n  return Object.assign(\n    {},\n    state,\n    Drive(state),\n    Refill(state),\n    ChangeName(state),\n    Fly(state),\n    Move(state),\n    LoggerIn(state)\n  );\n};\n\n  const myDrone = EcoRefillDrone('JS-Magic', 100);\n        myDrone.drive();\n        myDrone.refill();\n        myDrone.fly();\n        myDrone.move(100);\n        myDrone.move(100);\n\n        // console.log( 'myDrone', myDrone );\n        // myDrone.logger();\n  //\n  // const myDrone2 = EcoRefillDrone('JS-Is-Amaizing', 100);\n  // myDrone2.move(100);\n  // myDrone2.move(200);\n  // myDrone2.changeName('Dex3');\n        // myDrone2.changeName('HDMI');\n  //\n  // const Logger = obj => console.log( obj );\n  //       Logger(myDrone2);\n  //       myDrone2.logger();\n  //\n  //\n  //       let bindedMove = myDrone2.move.bind(null, 200);\n  //       MoveId.addEventListener('click', bindedMove );\n\n}\n\n\n// export default Composition;\n\n\n//# sourceURL=webpack:///./application/composition_vs_inheritance.js?");

/***/ }),

/***/ "./application/fabric.js":
/*!*******************************!*\
  !*** ./application/fabric.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n  /*\n\n      Сегодня разберем некоторые паттерны проектирования JS приложений.\n\n      Разберем паттерн \"Фабрика\" и то как он работает в JS.\n\n  */\n  const Fabric = () => {\n\n    class Weapon{\n\n      render(){\n        const root = document.getElementById('root');\n        const classMagic = this.magic ? 'magic' : 'regular';\n\n\n        root.innerHTML += `\n          <div class=\"${classMagic}\">\n            <h2>${this.name}</h2>\n            <span>${this.material}</span>\n            <div class=\"icon\">\n              <img src=\"${this.icon}\" width=\"100\" height=\"100\"/>\n            </div>\n          </div>\n        `;\n      }\n    }\n\n    class Sword extends Weapon{\n      constructor({name, material, style, magic}){\n        super();\n        this.weaponType = 'sword';\n        this.name = name || 'Unnamed sword';\n        this.material = material || 'Steel';\n        this.magic = magic !== undefined ? magic : false;\n        this.icon = 'images/swords.svg';\n      }\n    }\n\n    class Bow extends Weapon{\n      constructor({name, material, style, magic}){\n        super();\n        this.weaponType = 'bow';\n        this.name = name || 'Unnamed bow';\n        this.material = material || 'Wood';\n        this.magic = magic !== undefined ? magic : false;\n        this.icon = 'images/archery.svg';\n      }\n    }\n\n    class WeaponFactory {\n      makeWeapon( weapon ){\n        let WeaponClass = null;\n        if( weapon.weaponType === 'sword'){\n          WeaponClass = Sword;\n        } else if( weapon.weaponType === 'bow'){\n          WeaponClass = Bow;\n        } else {\n          return false;\n        }\n        return new WeaponClass( weapon );\n      }\n    }\n\n    const mySuperForge = new WeaponFactory();\n\n    const MakeMeBlade = mySuperForge.makeWeapon({\n      weaponType: 'sword',\n      name: 'winner',\n      metal: 'dark iron',\n      magic: true\n    })\n    const MakeMeBlade2 = mySuperForge.makeWeapon({\n      weaponType: 'sword',\n      name: 'defender',\n      metal: 'dark iron',\n      magic: true\n    })\n    const MakeMeBow = mySuperForge.makeWeapon({\n      weaponType: 'bow',\n      name: 'defender',\n      metal: 'dark iron',\n      magic: false\n    })\n\n    console.log( MakeMeBlade, MakeMeBlade2, MakeMeBow);\n    MakeMeBlade.render();\n    MakeMeBlade2.render();\n    MakeMeBow.render();\n    \n  }\n\n\n  /* harmony default export */ __webpack_exports__[\"default\"] = (Fabric);\n\n\n//# sourceURL=webpack:///./application/fabric.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _composition_vs_inheritance__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./composition_vs_inheritance */ \"./application/composition_vs_inheritance.js\");\n/* harmony import */ var _composition_vs_inheritance__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_composition_vs_inheritance__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _fabric__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fabric */ \"./application/fabric.js\");\n/* harmony import */ var _classwork_task1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classwork/task1 */ \"./classwork/task1.js\");\n/* harmony import */ var _classwork_task2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../classwork/task2 */ \"./classwork/task2.js\");\n\n\n\n\n\n// comp();\n// fabric();\n// taskOne();\nObject(_classwork_task2__WEBPACK_IMPORTED_MODULE_3__[\"default\"])();\n\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classwork/task1.js":
/*!****************************!*\
  !*** ./classwork/task1.js ***!
  \****************************/
/*! exports provided: BackendDeveloper, FrontendDeveloper, Designer, ProjectManager, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BackendDeveloper\", function() { return BackendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FrontendDeveloper\", function() { return FrontendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Designer\", function() { return Designer; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProjectManager\", function() { return ProjectManager; });\n/*\n  Composition:\n\n  Задание при помощи композиции создать объекты 4х типов:\n\n  functions:\n    - MakeBackendMagic\n    - MakeFrontendMagic\n    - MakeItLooksBeautiful\n    - DistributeTasks\n    - DrinkSomeTea\n    - WatchYoutube\n    - Procrastinate\n\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\n\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}\n\n*/\n\n  const MakeBackendMagic = person => ({\n    castBack: () => console.log(`Wow! ${person.name} is making backend magic!` )\n  });\n\n  const MakeFrontendMagic = person => ({\n    castFront: () => {\n      console.log(`${person.name} is making frontend magic` );\n    }\n  });\n\n  const MakeItLooksBeautiful = person => ({\n    beautify: () => console.log( `${person.name} makes it beutiful` )\n  });\n\n  const DistributeTasks = person => ({\n    distr: () => {\n      console.log( `${person.name} is distibuting tasks` );\n    }\n  });\n\n  const DrinkSomeTea = person => ({\n    drink: () => {\n      console.log( `${person.name} is drinking tea` );\n    }\n  });\n\n  const WatchYoutube = person => ({\n    watch: () => {\n      console.log( `${person.name} is watching YouTube` );\n    }\n  });\n\n  const Procrastinate = person => ({\n    nap: () => {\n      console.log( `${person.name} is procrastinating` )\n    }\n  });\n\nconst BackendDeveloper = (name, age, gender, rate) => {\n    let person = {\n      name,\n      age,\n      gender,\n      rate,\n      type: 'backend'\n    };\n\n    return Object.assign(\n    {},\n    person,\n    MakeBackendMagic(person),\n    DrinkSomeTea(person),\n    Procrastinate(person)\n    )\n  }\n\nconst FrontendDeveloper = (name, age, gender, rate) => {\n    let person = {\n      name,\n      age,\n      gender,\n      type: 'frontend'\n    };\n\n    return Object.assign(\n    {},\n    person,\n    MakeFrontendMagic(person),\n    DrinkSomeTea(person),\n    WatchYoutube(person)\n    )\n  }\n\nconst Designer = (name, age, gender, rate) => {\n    let person = {\n      name,\n      age,\n      gender,\n      type: 'design'\n    };\n\n    return Object.assign(\n    {},\n    person,\n    MakeItLooksBeautiful(person),\n    WatchYoutube(person),\n    Procrastinate(person)\n    )\n  }\n\nconst ProjectManager = (name, age, gender, rate) => {\n    let person = {\n      name,\n      age,\n      gender,\n      type: 'project'\n    };\n\n    return Object.assign(\n    {},\n    person,\n    DistributeTasks(person),\n    Procrastinate(person),\n    DrinkSomeTea(person)\n    )\n  }\n\n  let Petya = BackendDeveloper('Petya', 21, 'male');\n  Petya.castBack()\n\n  let Vasya = FrontendDeveloper('Vasya', 25, 'male');\n  Vasya.watch();\n\n  let Masha = Designer('Masha', 18, 'female');\n  Masha.nap();\n\n  let Katya = ProjectManager('Katya', 28, 'female');\n  Katya.distr();\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (BackendDeveloper);\n\n//# sourceURL=webpack:///./classwork/task1.js?");

/***/ }),

/***/ "./classwork/task2.js":
/*!****************************!*\
  !*** ./classwork/task2.js ***!
  \****************************/
/*! exports provided: taskTwo, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"taskTwo\", function() { return taskTwo; });\n/* harmony import */ var _classwork_task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/task1 */ \"./classwork/task1.js\");\n/*\n\n  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\n  расспределять и создавать сотрудников компании нужного типа.\n\n  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\n\n  HeadHunt => {\n    hire( obj ){\n      ...\n    }\n  }\n\n  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\n\n*/\n\n\n\n\n\nconst taskTwo = () => {\n\n\n    const fabric = list => {\n      const container = document.getElementById('root');\n      const table = document.createElement('table');\n      const title = document.createElement('h2');\n      const title2 = document.createElement('h2');\n      const div = document.createElement('div');   \n      const divSecond = document.createElement('div');\n      divSecond.id = \"result\";\n      title.innerHTML = `Доступные сотрудники:`;\n      title2.innerHTML = `Наша команда:`;\n      container.appendChild(div);\n      container.appendChild(divSecond);\n      div.appendChild(title);\n      div.appendChild(table);\n      divSecond.appendChild(title2);\n      let hiredArr = [];\n\n\n   function Render(){\n    const table = document.querySelector('table');    \n    list.map (person => {\n    let worker = HeadHuntFactory.hire(person);\n    let tr = document.createElement('tr');\n      tr.innerHTML = `\n        <td>\n            ${person.name} (${person.age})\n        </td>                \n        <td>\n            ${person.type}\n        </td>        \n        <td>\n            <button data-id=\"${person.name}\">Hire</button>\n        </td>\n        `\n      table.appendChild(tr);\n    const btn = table.querySelector(`button[data-id=\"${person.name}\"]`);\n          btn.addEventListener('click', hire);\n\n      function hire(){\n        let idx = list.indexOf(person);\n        const result = document.getElementById('result');\n        const table = document.createElement('table');\n        list.splice(idx,1);                \n        hiredArr.push(person);\n        hiredArr.map(person => {\n          table.innerHTML = `\n        <tr>\n          <td>${person.name}</td>\n          <td>${person.type}</td>\n          <td>${person.rate} $/hour</td>\n        </tr>\n        `\n        });\n        result.appendChild(table);\n      }  \n    });\n   }\n\n \n  class HeadHunt{\n    hire ( empl ){\n      let Employee = null;\n      if (empl.type === \"backend\"){\n        Employee = Object(_classwork_task1__WEBPACK_IMPORTED_MODULE_0__[\"BackendDeveloper\"])(empl.name, empl.age, empl.gender, empl.rate);       \n      } else if (empl.type === \"frontend\"){\n        Employee = Object(_classwork_task1__WEBPACK_IMPORTED_MODULE_0__[\"FrontendDeveloper\"])(empl.name, empl.age, empl.gender, empl.rate);\n      } else if (empl.type === \"design\"){\n        Employee = Object(_classwork_task1__WEBPACK_IMPORTED_MODULE_0__[\"Designer\"])(empl.name, empl.age, empl.gender, empl.rate);\n      } else if (empl.type === \"project\"){\n        Employee = Object(_classwork_task1__WEBPACK_IMPORTED_MODULE_0__[\"ProjectManager\"])(empl.name, empl.age, empl.gender, empl.rate);\n      } else {\n        return false;\n      }\n      return Employee;\n    }\n  }    \n\n    let HeadHuntFactory = new HeadHunt();\n    Render();\n  }\n   \n\n  let response = fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')\n    .then( res => res.json() )\n    .then( fabric );\n\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (taskTwo);\n\n//# sourceURL=webpack:///./classwork/task2.js?");

/***/ })

/******/ });