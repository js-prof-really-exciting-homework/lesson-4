/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}

*/

  const MakeBackendMagic = person => ({
    castBack: () => console.log(`Wow! ${person.name} is making backend magic!` )
  });

  const MakeFrontendMagic = person => ({
    castFront: () => {
      console.log(`${person.name} is making frontend magic` );
    }
  });

  const MakeItLooksBeautiful = person => ({
    beautify: () => console.log( `${person.name} makes it beutiful` )
  });

  const DistributeTasks = person => ({
    distr: () => {
      console.log( `${person.name} is distibuting tasks` );
    }
  });

  const DrinkSomeTea = person => ({
    drink: () => {
      console.log( `${person.name} is drinking tea` );
    }
  });

  const WatchYoutube = person => ({
    watch: () => {
      console.log( `${person.name} is watching YouTube` );
    }
  });

  const Procrastinate = person => ({
    nap: () => {
      console.log( `${person.name} is procrastinating` )
    }
  });

export  const BackendDeveloper = (name, age, gender, rate) => {
    let person = {
      name,
      age,
      gender,
      rate,
      type: 'backend'
    };

    return Object.assign(
    {},
    person,
    MakeBackendMagic(person),
    DrinkSomeTea(person),
    Procrastinate(person)
    )
  }

export  const FrontendDeveloper = (name, age, gender, rate) => {
    let person = {
      name,
      age,
      gender,
      type: 'frontend'
    };

    return Object.assign(
    {},
    person,
    MakeFrontendMagic(person),
    DrinkSomeTea(person),
    WatchYoutube(person)
    )
  }

export  const Designer = (name, age, gender, rate) => {
    let person = {
      name,
      age,
      gender,
      type: 'design'
    };

    return Object.assign(
    {},
    person,
    MakeItLooksBeautiful(person),
    WatchYoutube(person),
    Procrastinate(person)
    )
  }

export  const ProjectManager = (name, age, gender, rate) => {
    let person = {
      name,
      age,
      gender,
      type: 'project'
    };

    return Object.assign(
    {},
    person,
    DistributeTasks(person),
    Procrastinate(person),
    DrinkSomeTea(person)
    )
  }

  let Petya = BackendDeveloper('Petya', 21, 'male');
  Petya.castBack()

  let Vasya = FrontendDeveloper('Vasya', 25, 'male');
  Vasya.watch();

  let Masha = Designer('Masha', 18, 'female');
  Masha.nap();

  let Katya = ProjectManager('Katya', 28, 'female');
  Katya.distr();



export default BackendDeveloper;