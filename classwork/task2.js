/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/



import {BackendDeveloper, FrontendDeveloper, Designer, ProjectManager} from '../classwork/task1'

export const taskTwo = () => {


    const fabric = list => {
      const container = document.getElementById('root');
      const table = document.createElement('table');
      const title = document.createElement('h2');
      const title2 = document.createElement('h2');
      const div = document.createElement('div');   
      const divSecond = document.createElement('div');
      divSecond.id = "result";
      title.innerHTML = `Доступные сотрудники:`;
      title2.innerHTML = `Наша команда:`;
      container.appendChild(div);
      container.appendChild(divSecond);
      div.appendChild(title);
      div.appendChild(table);
      divSecond.appendChild(title2);
      let hiredArr = [];


   function Render(){
    const table = document.querySelector('table');    
    list.map (person => {
    let worker = HeadHuntFactory.hire(person);
    let tr = document.createElement('tr');
      tr.innerHTML = `
        <td>
            ${person.name} (${person.age})
        </td>                
        <td>
            ${person.type}
        </td>        
        <td>
            <button data-id="${person.name}">Hire</button>
        </td>
        `
      table.appendChild(tr);
    const btn = table.querySelector(`button[data-id="${person.name}"]`);
          btn.addEventListener('click', hire);

      function hire(){
        let idx = list.indexOf(person);
        const result = document.getElementById('result');
        const table = document.createElement('table');
        list.splice(idx,1);                
        hiredArr.push(person);
        hiredArr.map(person => {
          table.innerHTML = `
        <tr>
          <td>${person.name}</td>
          <td>${person.type}</td>
          <td>${person.rate} $/hour</td>
        </tr>
        `
        });
        result.appendChild(table);
      }  
    });
   }

 
  class HeadHunt{
    hire ( empl ){
      let Employee = null;
      if (empl.type === "backend"){
        Employee = BackendDeveloper(empl.name, empl.age, empl.gender, empl.rate);       
      } else if (empl.type === "frontend"){
        Employee = FrontendDeveloper(empl.name, empl.age, empl.gender, empl.rate);
      } else if (empl.type === "design"){
        Employee = Designer(empl.name, empl.age, empl.gender, empl.rate);
      } else if (empl.type === "project"){
        Employee = ProjectManager(empl.name, empl.age, empl.gender, empl.rate);
      } else {
        return false;
      }
      return Employee;
    }
  }    

    let HeadHuntFactory = new HeadHunt();
    Render();
  }
   

  let response = fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')
    .then( res => res.json() )
    .then( fabric );

};

export default taskTwo;